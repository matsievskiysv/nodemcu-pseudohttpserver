return function (connection, req, args)
   dofile("httpserver-header.lua")(connection, 200, args.ext, args.isGzipped)

   local children = {}
   local methods = {}
   local local_attrs = {}
   local private_attrs = {}
   local driver_attrs = {}
   for i in pairs(sdm.device_children(args.dev) or {}) do children[#children + 1] = i end
   for name, met in pairs(sdm.device_methods(args.dev) or {}) do
      if name:sub(1,1) ~= "_" then
         methods[#methods + 1] = {name=name, desc=met.desc}
      end
   end
   for name, att in pairs(sdm.device_local_attrs(args.dev) or {}) do
      if name:sub(1,1) ~= "_" then
         local_attrs[#local_attrs + 1] = {name=name, desc=att.desc, get=(att.get~=nil), set=(att.set~=nil)}
      end
   end
  for name, att in pairs(sdm.device_prvt_attrs(args.dev) or {}) do
      if name:sub(1,1) ~= "_" then
         private_attrs[#private_attrs + 1] = {name=name, desc=att.desc, get=(att.get~=nil), set=(att.set~=nil)}
      end
   end
  for name, att in pairs(sdm.driver_attrs(sdm.driver_attached(args.dev)) or {}) do
      if name:sub(1,1) ~= "_" then
         driver_attrs[#driver_attrs + 1] = {name=name, desc=att.desc, get=(att.get~=nil), set=(att.set~=nil)}
      end
   end

   local payload = {
      name = sdm.device_name(args.dev),
      driver = sdm.driver_name(sdm.driver_attached(args.dev)),
      methods = methods,
      local_attrs = local_attrs,
      private_attrs = private_attrs,
      driver_attrs = driver_attrs,
      children = children
   }
   local json = sjson.encode(payload)
   connection:send(json)
   children = nil
   methods = nil
   local_attrs = nil
   payload = nil
   json = nil
   collectgarbage()
end
