-- httpserver-static.lua
-- Part of nodemcu-httpserver, handles sending static files to client.
-- Author: Marcos Kirsch

return function (connection, req, args)
   dofile("httpserver-header.lua")(connection, 301, nil, false, {"Location: " .. args.location})
end
