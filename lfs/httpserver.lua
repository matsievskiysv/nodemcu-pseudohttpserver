-- httpserver
-- Author: Marcos Kirsch

-- Starts web server in the specified port.
return function (port)

   local s = net.createServer(net.TCP, 10) -- 10 seconds client timeout
   s:listen(
      port,
      function (connection)

         -- This variable holds the thread (actually a Lua coroutine) used for sending data back to the user.
         -- We do it in a separate thread because we need to send in little chunks and wait for the onSent event
         -- before we can send more, or we risk overflowing the mcu's buffer.
         local connectionThread

         local allowStatic = {GET=true, HEAD=true, POST=false, PUT=false, DELETE=false, TRACE=false, OPTIONS=false, CONNECT=false, PATCH=false}

         -- Pretty log function.
         local function log(connection, msg, optionalMsg)
            local port, ip = connection:getpeer()
            if(optionalMsg == nil) then
               print(ip .. ":" .. port, msg)
            else
               print(ip .. ":" .. port, msg, optionalMsg)
            end
         end

         local function startServing(fileServeFunction, connection, req, args)
            connectionThread = coroutine.create(function(fileServeFunction, bufferedConnection, req, args)
               fileServeFunction(bufferedConnection, req, args)
               -- The bufferedConnection may still hold some data that hasn't been sent. Flush it before closing.
               if not bufferedConnection:flush() then
                  log(connection, "closing connection", "no (more) data")
                  connection:close()
                  connectionThread = nil
                  collectgarbage()
               end
            end)

            local BufferedConnectionClass = dofile("httpserver-connection.lua")
            local bufferedConnection = BufferedConnectionClass:new(connection)
            local status, err = coroutine.resume(connectionThread, fileServeFunction, bufferedConnection, req, args)
            if not status then
               log(connection, "Error: "..err)
               log(connection, "closing connection", "error")
               connection:close()
               connectionThread = nil
               collectgarbage()
            end
         end

          local function handleRequest(connection, req)
            collectgarbage()
            local method = req.method
            local uri = req.uri
            local fileServeFunction = nil

            --[[
               not looking for files
               instead serving one index page for all calls
               special cases - style.css and script.js
               if url starts with /api, then serving json file
            --]]

            local nxt = uri.file:gmatch("[^/]+")
            local first = nxt()

            if allowStatic[method] then
               if first == "index.html" then
                  uri.args = {location = sdm.device_name(sdm.root())}
                  fileServeFunction = dofile("httpserver-redirect.lua")
               elseif first == "style.css" or
                  first == "script.js" or
                  first == "vue.js" or
                  first == "vue.min.js" or
               first == "favicon.ico" then
                  uri.file = "http" .. uri.file .. ".gz"
                  uri.args = {file = uri.file, ext = uri.ext, isGzipped = true}
                  fileServeFunction = dofile("httpserver-static.lua")
               else
                  local dev = sdm.root()
                  local name = first
                  if first == "api" then
                     name = nxt()
                  end
                  if name ~= sdm.device_name(dev) then dev = nil end
                  for name in nxt do
                     dev = sdm.device_child(dev, name)
                  end

                  if dev ~= nil then
                     if first == "api" then
                        if uri.args.call == nil then
                           uri.args = {ext = "json", isGzipped = false, dev = dev}
                           fileServeFunction = dofile("httpserver-sdmdev.lua")
                        else
                           uri.args = {ext = "json", isGzipped = false, dev = dev, args = uri.args}
                           fileServeFunction = dofile("httpserver-sdmcall.lua")
                        end
                     else
                        uri.file = "http/index.html.gz"
                        uri.ext = "html"
                        uri.args = {file = uri.file, ext = uri.ext, isGzipped = true}
                        fileServeFunction = dofile("httpserver-static.lua")
                     end
                  else
                     uri.args = {code = 404, errorString = "Not Found", logFunction = log}
                     fileServeFunction = dofile("httpserver-error.lua")
                  end
               end
            else
               uri.args = {code = 405, errorString = "Method not supported", logFunction = log}
               fileServeFunction = dofile("httpserver-error.lua")
            end

            print("Serving ... free heap: ", node.heap())
            startServing(fileServeFunction, connection, req, uri.args)
         end

         local function onReceive(connection, payload)
            collectgarbage()
            local conf = require "httpserver-conf"
            local auth
            local user = "Anonymous"

            -- as suggest by anyn99 (https://github.com/marcoskirsch/nodemcu-httpserver/issues/36#issuecomment-167442461)
            -- Some browsers send the POST data in multiple chunks.
            -- Collect data packets until the size of HTTP body meets the Content-Length stated in header
            if payload:find("Content%-Length:") or bBodyMissing then
               if fullPayload then fullPayload = fullPayload .. payload else fullPayload = payload end
               if (tonumber(string.match(fullPayload, "%d+", fullPayload:find("Content%-Length:")+16)) > #fullPayload:sub(fullPayload:find("\r\n\r\n", 1, true)+4, #fullPayload)) then
                  bBodyMissing = true
                  return
               else
                  --print("HTTP packet assembled! size: "..#fullPayload)
                  payload = fullPayload
                  fullPayload, bBodyMissing = nil
               end
            end
            collectgarbage()

            -- parse payload and decide what to serve.
            local req = dofile("httpserver-request.lua")(payload)
            log(connection, req.method, req.request)
            if conf.auth.enabled then
               auth = dofile("httpserver-basicauth.lua")
               user = auth.authenticate(payload) -- authenticate returns nil on failed auth
            end

            if user and req.methodIsValid and (req.method == "GET" or req.method == "POST" or req.method == "PUT") then
               req.user = user
               handleRequest(connection, req, handleError)
            else
               local args = {}
               local fileServeFunction = dofile("httpserver-error.lua")
               if not user then
                  args = {code = 401, errorString = "Not Authorized", headers = {auth.authErrorHeader()}, logFunction = log}
               elseif req.methodIsValid then
                  args = {code = 501, errorString = "Not Implemented", logFunction = log}
               else
                  args = {code = 400, errorString = "Bad Request", logFunction = log}
               end
               startServing(fileServeFunction, connection, req, args)
            end
         end

         local function onSent(connection, payload)
            collectgarbage()
            if connectionThread then
               local connectionThreadStatus = coroutine.status(connectionThread)
               if connectionThreadStatus == "suspended" then
                  -- Not finished sending file, resume.
                  local status, err = coroutine.resume(connectionThread)
                  if not status then
                     log(connection, "Error: "..err)
                     log(connection, "closing connection", "error")
                     connection:close()
                     connectionThread = nil
                     collectgarbage()
                  end
               elseif connectionThreadStatus == "dead" then
                  -- We're done sending file.
                  log(connection, "closing connection","thread is dead")
                  connection:close()
                  connectionThread = nil
                  collectgarbage()
               end
            end
         end

         local function onDisconnect(connection, payload)
-- this should rather be a log call, but log is not available here
--            print("disconnected")
            if connectionThread then
               connectionThread = nil
               collectgarbage()
            end
         end

         connection:on("receive", onReceive)
         connection:on("sent", onSent)
         connection:on("disconnection", onDisconnect)

      end
   )
   return s

end
